var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var bodyParser = require('body-parser')

app.use(bodyParser.json()); // for parsing application/json

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/notify', function(req, res) {
    io.emit('stock update', [{
        "name": "regular",
        "price": "100",
        "quantity": 50,
        "key": "q"
    }, {
        "name": "premium",
        "price": "200",
        "quantity": 50,
        "key": "w"
    }, {
        "name": "VIP",
        "price": "1000",
        "quantity": 10,
        "key": "e"
    }, {
        "name": "Platinium",
        "price": "600",
        "quantity": 10,
        "key": "r"
    }]);
    res.send('hello world');
});

app.post('/notify', function(req, res) {
    io.emit('stock update', req.body);
    console.log(req.body);
    res.json(req.body);
});



io.on('connection', function(socket) {
    socket.on('chat message', function(msg) {
        io.emit('chat message', msg);
    });
});

http.listen(3000, function() {
    console.log('listening on *:3000');
});
